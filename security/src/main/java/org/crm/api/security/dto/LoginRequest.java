package org.crm.api.security.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class LoginRequest {

    @NotBlank()
    @Size(max = 40)
    private String login;

    @NotBlank()
    @Size(min = 4, max = 100)
    private String password;

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
