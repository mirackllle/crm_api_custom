package org.crm.api.security.message;

public enum Message {

    SEND_MAIL_ERROR("Error sending html message!"),
    USER_NOT_FOUND_ERROR("User not found!"),
    SECURITY_CONTEXT_ERROR("Could not set user authentication in security context!"),
    ACTIVATION_USER_ERROR("The account has already been activated!"),
    LOGIN_USER_ERROR("User is not activated!"),
    REGISTRATION_USER_ERROR("Email address already in use!"),
    REFRESH_TOKEN_ERROR("Invalid Refresh Token! User not found by refreshToken"),
    UNAUTHORIZED_ERROR("You are not authorized to access this resource!"),
    INVALID_TOKEN_ERROR("Invalid JWT token!"),
    INVALID_PASSWORD("Invalid password!"),

    SUCCESS_REGISTRATION("User registered successfully!"),
    SUCCESS_UPDATE_USER("User data updated successfully!"),
    SUCCESS_UPDATE_SCHEDULE("Schedule data updated successfully!"),
    SUCCESS_ADD_AUDITORY("Auditory added successfully!"),
    SUCCESS_UPDATE_AUDITORY("Auditory updated successfully!"),
    SUCCESS_REMOVE_AUDITORY("Auditory removed successfully!"),
    SUCCESS_UPDATE_PASSWORD("Password updated successfully!");

    private String description;

    Message(final String description){
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

}
