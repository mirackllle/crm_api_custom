package org.crm.api.security.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ResponseStatus(code = BAD_REQUEST)
public class UserSignInException extends RuntimeException {

    public UserSignInException(final String message) {
        super(message);
    }
}
