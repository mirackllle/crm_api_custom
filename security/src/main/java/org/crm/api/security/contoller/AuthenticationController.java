package org.crm.api.security.contoller;

import org.crm.api.security.dto.LoginRequest;
import org.crm.api.security.service.AuthServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {

    private final transient AuthServiceImpl authService;

    @Autowired
    AuthenticationController (final AuthServiceImpl authService) {
        this.authService = authService;
    }

    @PostMapping("/singin")
    public ResponseEntity<?> singin(@Valid @RequestBody final LoginRequest authRequest,
                                    final HttpServletResponse response) {
        Float f = new Float("10");
        System.out.println(f);
        return status(OK).body(authService.login(authRequest, response));
    }
}
