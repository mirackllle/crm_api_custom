package org.crm.api.security.service;

import org.crm.api.security.config.HeaderProperties;
import org.crm.api.security.config.JwtProperties;
import org.crm.api.security.dto.JwtAuthenticationResponse;
import org.crm.api.security.dto.LoginRequest;
import org.crm.api.security.entity.User;
import org.crm.api.security.security.JwtProvider;
import org.crm.api.security.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.net.HttpURLConnection;
//import java.net.MalformedURLException;
//import java.net.ProtocolException;
//import java.net.URL;
import java.time.LocalDate;

@Service
public class AuthServiceImpl {

    private final transient JwtProvider jwtProvider;
    private final transient JwtProperties jwtProperties;
    private final transient HeaderProperties headerProperties;

    @Autowired
    AuthServiceImpl (final JwtProvider jwtProvider, final JwtProperties jwtProperties,
                     final HeaderProperties headerProperties) {
        this.jwtProperties = jwtProperties;
        this.jwtProvider = jwtProvider;
        this.headerProperties = headerProperties;

    }

    public JwtAuthenticationResponse login (final LoginRequest loginRequest,
                                            final HttpServletResponse response) {
        //            URL url = new URL("http://crm.viasat.ua/crm/services/CRMWSApi?WSDL");
//            HttpURLConnection con = (HttpURLConnection) url.openConnection();
//            con.setRequestMethod("POST");
        if (true) {
            final UserPrincipal userPrincipal = UserPrincipal.create(new User(loginRequest.getLogin(), loginRequest.getPassword()));
            final String accessToken = jwtProvider.generateAccessToken(userPrincipal);
                response.setHeader(headerProperties.getName(), headerProperties.getType() + accessToken);
                return new JwtAuthenticationResponse(accessToken, headerProperties.getType().trim(),
                        null, LocalDate.now(), jwtProperties.getExpirationInMs());
        }
        return null;
    }

}
