package org.crm.api.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan("org.crm.api")
@SpringBootApplication(scanBasePackages = "org.crm.api.security")
public class SecurityAplicationRunner {

    public static void main(final String[] args) {
        SpringApplication.run(SecurityAplicationRunner.class, args);
    }

}
