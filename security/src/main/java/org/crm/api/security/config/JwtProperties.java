package org.crm.api.security.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;

@Component
@ConfigurationProperties(prefix = "app.jwt")
public class JwtProperties {

    @NotBlank
    private String secretKey;

    @NotBlank
    private Long expirationInMs;

    public String getSecretKey() {
        return secretKey;
    }

    public Long getExpirationInMs() {
        return expirationInMs;
    }

    public void setSecretKey(final String secretKey) {
        this.secretKey = secretKey;
    }

    public void setExpirationInMs(final Long expirationInMs) {
        this.expirationInMs = expirationInMs;
    }
}
