package org.crm.api.security.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.crm.api.security.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class UserPrincipal implements UserDetails {

    @JsonIgnore
    private transient final String login;

    @JsonIgnore
    private transient final String password;

    private transient final boolean isEnabled;

    public static UserPrincipal create(final User user) {

        return new UserPrincipal(
                user.getLogin(),
                user.getPassword(),
                true
        );
    }

    public UserPrincipal(final String login, final String password, final boolean isEnabled) {
        this.login = login;
        this.password = password;
        this.isEnabled = isEnabled;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnabled;
    }
}
