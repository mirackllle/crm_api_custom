package org.crm.api.security.dto;

import java.time.LocalDate;

public class JwtAuthenticationResponse {

    private String accessToken;
    private String tokenType;
    private String refreshToken;
    private LocalDate dateCreate;
    private Long expiresInMsec;

    public JwtAuthenticationResponse(String accessToken, String tokenType, String refreshToken, LocalDate dateCreate, Long expiresInMsec) {
        this.accessToken = accessToken;
        this.tokenType = tokenType;
        this.refreshToken = refreshToken;
        this.dateCreate = dateCreate;
        this.expiresInMsec = expiresInMsec;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public LocalDate getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(LocalDate dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Long getExpiresInMsec() {
        return expiresInMsec;
    }

    public void setExpiresInMsec(Long expiresInMsec) {
        this.expiresInMsec = expiresInMsec;
    }
}
