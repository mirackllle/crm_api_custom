package org.crm.api;

import org.crm.api.security.SecurityAplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Import;

@EntityScan("org.crm.api")
@SpringBootApplication(scanBasePackages = "org.crm.api")
@Import({SecurityAplicationRunner.class})
public class ApplicationRunner {

    public static void main(final String[] args) {
        SpringApplication.run(ApplicationRunner.class, args);
    }

}
