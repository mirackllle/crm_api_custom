package org.crm.api.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "filter")
@EnableConfigurationProperties
public class LogConfigurationProperty {

    private boolean logBody;

    public boolean isLogBody() {
        return logBody;
    }

    public void setLogBody(final boolean logBody) {
        this.logBody = logBody;
    }
}