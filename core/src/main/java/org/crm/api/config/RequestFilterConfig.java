package org.crm.api.config;

import org.crm.api.filter.CustomRequestResponseLoggingFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.OncePerRequestFilter;

@Configuration
@SuppressWarnings("PMD")
public class RequestFilterConfig {

    private final LogConfigurationProperty logConfigurationProperty;

    @Autowired
    public RequestFilterConfig(final LogConfigurationProperty property) {
        this.logConfigurationProperty = property;
    }

    @Bean
    public OncePerRequestFilter responseLoggingFilter(){
        return new CustomRequestResponseLoggingFilter(logConfigurationProperty.isLogBody());
    }

}
