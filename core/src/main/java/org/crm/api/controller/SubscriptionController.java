package org.crm.api.controller;

import org.crm.api.security.security.UserPrincipal;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/api/subscription")
public class SubscriptionController {

    @GetMapping("/create")
    public ResponseEntity<?> create(@AuthenticationPrincipal final UserPrincipal principal) {
        return status(OK).body("Test");
    }
}
